<?php
Class Feed_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function get_all_items($sort_by, $page, $items_per_page) {
		$sql = '
			SELECT fi.id, item_url, fi.title, fi.description, date, feed_id, f.title AS feed_title,
				TIMESTAMPDIFF(MINUTE, date, NOW()) AS last_updated,
				CASE WHEN s.id IS NULL THEN 0 ELSE 1 END AS is_starred
			FROM feed_items AS fi
			LEFT OUTER JOIN feeds AS f
				ON fi.feed_id = f.id
			LEFT OUTER JOIN starred AS s
				ON s.id = fi.id
			ORDER BY last_updated ' . $sort_by . '
			LIMIT ? OFFSET ?;';

		$offset = ($page - 1) * $items_per_page;
		$result = $this->db->query($sql, array($items_per_page, $offset))->result();

		foreach ($result as $row) {
			$row->last_updated = $this->minutes_to_human_readable($row->last_updated);
		}

		return $result;
	}

	function get_subs() {
		$sql = '
			SELECT id, feed_url AS url
			FROM feeds
			ORDER BY id;';

		return $this->db->query($sql)->result();
	}

	function get_feeds() {
		$sql = '
			SELECT id, title, latest, last_updated, description
			FROM feeds
			LEFT OUTER JOIN (
				SELECT feed_id, MAX(date) AS latest, TIMESTAMPDIFF(MINUTE, MAX(date), NOW()) AS last_updated
				FROM feed_items
				GROUP BY feed_id
			) AS fi
			ON feeds.id = fi.feed_id;';

		$result = $this->db->query($sql)->result();

		foreach ($result as $row) {
			$row->last_updated = $this->minutes_to_human_readable($row->last_updated);
		}

		return $result;
	}

	function get_feed_info($id) {
		$sql = '
			SELECT *
			FROM feeds
			WHERE id = ?;';

		return $this->db->query($sql, array($id))->row();
	}

	function get_feed_items($id, $sort_by, $page, $items_per_page) {
		$sql = '
			SELECT fi.id, item_url, fi.title, fi.description, date, feed_id,
				TIMESTAMPDIFF(MINUTE, date, NOW()) AS last_updated,
				CASE WHEN s.id IS NULL THEN 0 ELSE 1 END AS is_starred
			FROM feed_items AS fi
			LEFT OUTER JOIN starred AS s
				ON s.id = fi.id
			WHERE feed_id = ?
			ORDER BY last_updated ' . $sort_by . '
			LIMIT ? OFFSET ?;';

		$offset = ($page - 1) * $items_per_page;
		$result = $this->db->query($sql, array($id, $items_per_page, $offset))->result();

		foreach ($result as $row) {
			$row->last_updated = $this->minutes_to_human_readable($row->last_updated);
		}

		return $result;
	}

	function get_item_count($feed_id = false) {
		$where_clause = ($feed_id !== false) ? 'WHERE feed_id = ?' : '';
		$sql = '
			SELECT *
			FROM feed_items
			'. $where_clause . ';';

		$query = $this->db->query($sql, array($feed_id));

		return $query->num_rows();
	}

	private function minutes_to_human_readable($min) {
		if ($min === null) {
			return 'never';
		} else if ($min >= 2880) {
			return '(' . round($min / 1440) . ' days ago)';
		} else if ($min >= 1440) {
			return '(' . 'a day ago)';
		} else if ($min >= 120) {
			return '(' . round($min / 60) . ' hours ago)';
		} else if ($min >= 60) {
			return '(' . 'an hour ago)';
		} else if ($min >= 5) {
			return '(' . $min . ' minutes ago)';
		} else {
			return '(just now)';
		}
	}
}
