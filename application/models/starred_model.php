<?php
Class Starred_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function get_all_starred() {
		$sql = '
			SELECT fi.id, item_url, fi.title, fi.description, date, feed_id, f.title AS feed_title,
				TIMESTAMPDIFF(MINUTE, date, NOW()) AS last_updated
			FROM starred AS s
			LEFT OUTER JOIN feed_items AS fi
				ON s.id = fi.id
			LEFT OUTER JOIN feeds AS f
				ON fi.feed_id = f.id
			ORDER BY s.date_added DESC; ';

		$result = $this->db->query($sql)->result();

		foreach ($result as $row) {
			$row->last_updated = $this->minutes_to_human_readable($row->last_updated);
		}

		return $result;
	}

	function star_item($item_id) {
		$sql = 'INSERT IGNORE INTO starred VALUES (?, NOW());';

		$this->db->query($sql, array($item_id));
	}

	function unstar_item($item_id) {
		$sql = 'DELETE FROM starred WHERE id = ?;';

		$this->db->query($sql, array($item_id));
	}

	private function minutes_to_human_readable($min) {
		if ($min === null) {
			return 'never';
		} else if ($min >= 2880) {
			return '(' . round($min / 1440) . ' days ago)';
		} else if ($min >= 1440) {
			return '(' . 'a day ago)';
		} else if ($min >= 120) {
			return '(' . round($min / 60) . ' hours ago)';
		} else if ($min >= 60) {
			return '(' . 'an hour ago)';
		} else if ($min >= 5) {
			return '(' . $min . ' minutes ago)';
		} else {
			return '(just now)';
		}
	}
}
