<div id="sidebar">
	<div id="logo"><span class="fa fa-rss-square fa-3x"></span></div>
	<div class="sidebar-nav"><a href="<?= site_url('feeds') ?>">All Items</a></div>
	<div class="sidebar-nav"><a href="<?= site_url('feeds/subscribed') ?>">Subscriptions</a></div>
	<div class="sidebar-nav"><a href="<?= site_url('starred') ?>">Starred</a></div>
	<hr>

</div>
