<div id="content">
	<div class="header">
		<h2>Subscriptions</h2>
	</div>

	<?php foreach ($feeds as $item): ?>
		<div class="item">
			<h3><a href="<?= site_url('feeds/' . $item->id) ?>"><?= $item->title ?></a></h3>
			<p><small><?= $item->description ?></small></p>
			<p><small>Last updated: <?= $item->latest ?> <?= $item->last_updated ?></small></p>
		</div>
	<?php endforeach ?>
</div>
