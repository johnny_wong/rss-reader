<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

	<title>RSS Feed</title>

	<script type="text/javascript" src="/res/jquery-2.1.3.min.js"></script>
	<script type="text/javascript">
		$(window).ready(function () {
			// stars/unstars items
			$(".item-content-star").click(function () {
				if ($(this).hasClass("item-unstarred")) {
					$(this).removeClass("item-unstarred");
					$(this).addClass("item-starred");
					$(this).html('<span class="fa fa-star fa-fw"></span>');
					$.get("/index.php/starred/star/" + $(this).attr("item-id"));
				} else if ($(this).hasClass("item-starred")) {
					$(this).removeClass("item-starred");
					$(this).addClass("item-unstarred");
					$(this).html('<span class="fa fa-star-o fa-fw"></span>');
					$.get("/index.php/starred/unstar/" + $(this).attr("item-id"));
				}
			});

			// expands/collapses feed items
			$(".item-content-toggle").click(function() {
				content_toggle($(this));
			});

			$(".item-content-expand-all").click(function() {
				$(".item-content-toggle").each(function() {
					if ($(this).children(":first").hasClass("fa-expand")) {
						content_toggle($(this));
					}
				});
			});

			$(".item-content-collapse-all").click(function() {
				$(".item-content-toggle").each(function() {
					if ($(this).children(":first").hasClass("fa-compress")) {
						content_toggle($(this));
					}
				});
			});

			// force all links in feed item contents to open in new tab/window
			$(function() {
			  $(".item-content a").attr("target", "_blank");
			});
		});

		function content_toggle($header) {
			$content = $header.parent().siblings(".item-content");

			$content.slideToggle(150, function() {
				$header.html(function() {
					return $header.children(":first").hasClass("fa-expand") ? '<span class="fa fa-compress fa-fw"></span>' : '<span class="fa fa-expand fa-fw"></span>';
				});
			});
		}
	</script>

	<link rel="stylesheet" href="/res/fa/css/font-awesome.min.css">
	<style type="text/css">
	/* color scheme:
	 * white		#ECF0F1
	 * darkkblue	#2C3E50
	 * blue			#2980B9
	 * lightblue	#3498DB
	 * red			#E74C3C
	 */

	@font-face {
		font-family: 'Roboto Slab';
		src: url('/res/RobotoSlab.ttf');
	}

	body {
		color: #2C3E50;
		background-image: linear-gradient(to right, #2980B9, #2980B9 250px, #ECF0F1 250px, #ECF0F1 100%);
		margin: 0;
		text-rendering: geometricPrecision;
	}

	/*
	 * Sidebar
	 */
	
	#sidebar {
		font-family: 'Roboto Slab', serif;
		color:#ECF0F1;
		margin: 0px;
		box-sizing: border-box;
		float: left;
		width: 250px;
		padding-left: 25px;
		padding-right: 25px;
		font-size: 1em;
	}

	#sidebar #logo { padding-bottom: 25px; text-align: center; }

	#sidebar hr { border: 0; height: 1px; background-color: #ECF0F1; }

	#sidebar a { color: #ECF0F1; text-decoration: none; }

	#sidebar a:hover { color:#E74C3C; }

	.sidebar-nav { padding-bottom: 5px; }

	/*
	 * Content
	 */

	#content {
		font: 12px/1.4em 'Roboto Slab', serif;
		margin-left: 270px;
		margin-right: 20px;
		margin-top: 30px;
	}

	#toolbar {
		list-style: none;
		position: fixed;
		top: 20px;
		right: 20px;
	}

	ul#toolbar li {
		display: inline;
		padding: 5px 9px 5px 5px;
		color: #ECF0F1;
		background: #3498DB;
		border-radius: 3px;
		border: 1px solid #3498DB;
		cursor: pointer;
	}

	ul#toolbar li:hover {
		color: #3498DB;
		background: #ECF0F1;
		border: 1px solid #3498DB;
	}

	.item-header { width: 100%; padding: 5px; }

	.item-content { display: none; padding-left: 30px; }

	.item-content-icon { padding-right: 5px; cursor: pointer; }

	.fa-star { color: #ff8d00; }

	.fa-external-link { font-size: 10px; }

	.item-header h3 { display: inline; }

	h1 { font-size: 2.2em; }

	div.header { border-bottom:1px solid #999; }

	div.item { padding:5px 0; border-bottom:1px solid #999; }

	a { color: #2980B9; text-decoration: none; }

	a:hover { color:#E74C3C; }

	#pages { text-align: center; font-weight: bold; }

	</style>

</head>
<body>
