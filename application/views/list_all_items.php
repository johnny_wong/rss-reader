<div id="content">
	<div class="header">
		<h2>All Items</h2>
		<?= $toolbar ?>
	</div>

	<?php foreach ($feed_items as $item): ?>
		<div class="item">
			<div class="item-header">
				<span class="item-content-star item-<?= ($item->is_starred) ? '' : 'un' ?>starred item-content-icon" title="Star/Unstar" item-id="<?= $item->id ?>"><span class="fa fa-star<?= ($item->is_starred) ? '' : '-o' ?> fa-fw"></span></span>
				<span class="item-content-toggle item-content-icon" title="Expand/Collapse"><span class="fa fa-expand fa-fw"></span></span>
				<h3><a href="<?= $item->item_url ?>" target="_blank"><?= $item->title ?> <span class="fa fa-external-link fa-fw"></span></a></h3>
			</div>
			<div class="item-content">
				<p><?= $item->description ?></p>
				<p><small>Posted in <span class="tag"><a href="<?= site_url('feeds/' . $item->feed_id) ?>"><?= $item->feed_title ?></a></span> on <?= $item->date ?> <?= $item->last_updated ?></small></p>
			</div>
		</div>
	<?php endforeach ?>
	<p id="pages"><?= $pages ?></p>
</div>
