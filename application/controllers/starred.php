<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Starred extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('starred_model', '', TRUE);
	}

	function index() {
		$data['toolbar'] = $this->load->view('toolbar_view', null, true);
		$data['starred_items'] = $this->starred_model->get_all_starred();

		$this->load->view('header_view');
		$this->load->view('sidebar_view');
		$this->load->view('list_all_starred', $data);
		$this->load->view('footer_view');
	}

	function star($item_id) {
		$this->starred_model->star_item($item_id);
	}

	function unstar($item_id) {
		$this->starred_model->unstar_item($item_id);
	}
}
