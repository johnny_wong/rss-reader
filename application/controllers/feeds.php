<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feeds extends CI_Controller {

	private $items_per_page;

	function __construct() {
		parent::__construct();
		$this->load->model('feed_model', '', TRUE);
		$this->load->helper('url');
		$this->load->library('pagination');

		$this->items_per_page = 20;
	}

	function index() {
		redirect(site_url('feeds/all'));
	}

	function all() {
		list($sort_by, $page, $items_per_page) = $this->_get_params();
		$this->_add_pagination('all', $this->feed_model->get_item_count());

		$items = $this->feed_model->get_all_items($sort_by, $page, $items_per_page);

		$data['toolbar'] = $this->load->view('toolbar_view', null, true);
		$data['feed_items'] = $items;
		$data['pages'] = $this->pagination->create_links();

		$this->load->view('header_view');
		$this->load->view('sidebar_view');
		$this->load->view('list_all_items', $data);
		$this->load->view('footer_view');
	}

	function subscribed() {
		$feeds = $this->feed_model->get_feeds();

		$data['feeds'] = $feeds;

		$this->load->view('header_view');
		$this->load->view('sidebar_view');
		$this->load->view('list_feeds', $data);
		$this->load->view('footer_view');
	}

	function show($feed_id) {
		list($sort_by, $page, $items_per_page) = $this->_get_params();
		$this->_add_pagination($feed_id, $this->feed_model->get_item_count($feed_id));

		$feed_info = $this->feed_model->get_feed_info($feed_id);
		$feed_items = $this->feed_model->get_feed_items($feed_id, $sort_by, $page, $items_per_page);

		$data['toolbar'] = $this->load->view('toolbar_view', null, true);
		$data['feed_url'] = $feed_info->feed_url;
		$data['feed_title'] = $feed_info->title;
		$data['feed_desc'] = $feed_info->description;
		$data['feed_items'] = $feed_items;
		$data['pages'] = $this->pagination->create_links();

		$this->load->view('header_view');
		$this->load->view('sidebar_view');
		$this->load->view('list_feed_items', $data);
		$this->load->view('footer_view');
	}

	private function _get_params() {
		$params = $this->uri->uri_to_assoc(3, array('sort-by', 'page'));

		if ($params['sort-by'] === false) {
			$params['sort-by'] = 'asc'; # default sort order
		} else if (strtolower($params['sort-by']) !== 'asc' and strtolower($params['sort-by']) !== 'desc') {
			show_404();
		}

		if ($params['page'] === false) {
			$params['page'] = 1; # default page
		} else if (preg_match('/^0*(\d+)$/', $params['page'], $match)) {
			$params['page'] = intval($match[1]);
		} else {
			show_404();
		}

		return array($params['sort-by'], $params['page'], $this->items_per_page);
	}

	private function _add_pagination($cont_method, $total) {
		$config['base_url'] = site_url('feeds/' . $cont_method . '/page/');
		$config['uri_segment'] = 4;
		$config['total_rows'] = ceil($total / $this->items_per_page);
		$config['per_page'] = 1;
		$config['use_page_numbers'] = true;
		$config['first_link'] = '';
		$config['last_link'] = '';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;<span>';
		$config['next_tag_close'] = '</span>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<span>';
		$config['prev_tag_close'] = '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
		$config['display_pages'] = false;

		$this->pagination->initialize($config);
	}
}
