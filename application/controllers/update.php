<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Update extends CI_Controller {

	function __construct() {
		parent::__construct();
		require_once('SimplePie.compiled.modified.php');
		require_once('rss_conf.php');
		$this->load->model('feed_model', '', TRUE);
	}

	function index() {
		foreach ($this->feed_model->get_subs() as $subscription) {
			$feed = new SimplePie();
			$feed->set_feed_url($subscription->url);
			$feed->set_cache_location('mysql://' . DB_USER . ':' . DB_PASS . '@' . DB_HOST . ':' . DB_PORT . '/' . DB_NAME);
			$feed->set_cache_duration(0);
			$success = $feed->init();
			$feed->handle_content_type();

			echo $feed->get_title() . "<br><br>";
			foreach($feed->get_items() as $item) {
				$data['hash'] = $item->get_id();
				if ($this->db->get_where('feed_items', array('hash' => $data['hash']))->num_rows() == 0) {
					$data['item_url'] = $item->get_permalink();
					$data['feed_id'] = $subscription->id;
					$data['title'] = $item->get_title();
					$data['description'] = $item->get_description();
					$data['date'] = $item->get_date('Y-m-d H:i:s');

					echo $data['feed_id'] . " | " . $data['item_url'] . " | " . $data['title'] . " | " . $data['description'] . " | " . $data['date'] . "<br>";

					$this->db->insert('feed_items', $data);
				}
			}
			$feed->__destruct();
		}
	}
}
