Instructions will be brief and unautomated while this is such a work in progress.

1. Get CodeIgniter
2. Clone repo and move applications/ into CodeIgniter folder where appropriate
3. In the autoload config, add 'database' to libraries and 'url' to helper
4. Add the following to routes config

`route['feeds/(:num)'] = "feeds/show/$1";`

`$route['feeds/(:num)/(.*)'] = "feeds/show/$1/$2";`

5. Create user on MySQL and a databse called 'ci'
6. Fill in databases config with your own details
7. Fill out application/controllers/rss_conf.php with similar MySQL info
8. Import schema with `mysql -u <user> -p ci < schema.sql`
9. Add feeds to db, for now `insert into feeds values (null, 'http://www/example.com/url/of/rss', 'Name Of Feed', 'Feed description');` is the only way
10. Run application/controllers/update.php, ideally set it as an hourly cronjob or something

